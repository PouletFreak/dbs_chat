import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.Properties;
import java.util.Scanner;

/**
 * In the class Main a connection to the database "hotel_rot" is established.
 * Using this connection, a query that outputs all the bookings an relevant
 * attributes is prepared and executed. The time period of which the booking
 * should be displayed needs to be specified via the console once the program
 * has been started.
 * 
 * @author Giancarlo Bergamin
 * @author Yvo Broennimann
 * @author Melanie Calame
 * @author Anna-Lena Klaus
 * @version 1.0
 *
 */
public class Main {

	/**
	 * In the main method the connection is established, the query prepared and the user prompted for a start and
	 * end date. After the user has provided these dates, they are inserted into the query which is then executed.
	 * The output is formatted and printed to the console.
	 * @param args
	 *            none required
	 */
	public static void main(String[] args) {
		
		//Loading properties such as the url, the username and the password required to connect to the database from the config.properties file
		Properties properties = new Properties();
		try {
			properties.load(Thread.currentThread().getContextClassLoader().getResourceAsStream("config.properties"));
		} catch (IOException e1) {
			System.out.println(e1);
			e1.printStackTrace();
		}
		String connectString = properties.getProperty("jdbc.url");
		String user = properties.getProperty("jdbc.username");
		String password = properties.getProperty("jdbc.password");

		try {
			Connection con = DriverManager.getConnection(connectString, user, password);
			String query = "select distinct buchung.buchung_buchungsnr, person.vorname, person.nachname, buchung.von, buchung.bis, buchung.anzahl_personen, zimmer.zimmer_zimmernr from buchung "
					+ "join person on person.person_id=buchung.person_id "
					+ "join buchung_zimmer on buchung_zimmer.buchung_buchungsnr=buchung.buchung_buchungsnr "
					+ "join zimmer on buchung_zimmer.zimmer_zimmernr=zimmer.zimmer_zimmernr "
					+ "join buchungsstatus on buchung.buchung_buchungsnr = buchungsstatus.buchung_buchungsnr "
					+ "where not buchungsstatus.bestaetigung is null " + "and buchungsstatus.cancelled is null "
					+ "and von > ? and bis < ?";
			PreparedStatement stmt = con.prepareStatement(query); // Statement wird an DB geschickt und von der DB geprueft. Es wird vorkompiliert, falls es richtig ist
								
			System.out.println("Enter von date (yyyy-mm-dd)");
			Scanner sc = new Scanner(System.in);

			Boolean correct = true;
			while (correct) {
				try {
					String inputVon = sc.next();
					stmt.setDate(1, java.sql.Date.valueOf(inputVon));
					correct = false;
				} catch (java.lang.IllegalArgumentException e) {
					System.out.println("Your date has the wrong format enter a new one");
				}
			}

			System.out.println("Enter bis date (yyyy-mm-dd)");
			correct = true;
			while (correct) {
				try {
					String inputBis = sc.next();
					stmt.setDate(2, java.sql.Date.valueOf(inputBis));
					correct = false;
				} catch (java.lang.IllegalArgumentException e) {
					System.out.println("Your date has the wrong format enter a new one");
				}
			}

			sc.close();
			ResultSet res = stmt.executeQuery(); // Fuehrt query wieder normal aus

			System.out.println(
					"************************************************************************************************************");
			System.out.println("Buchungsnummer\tVorname\t\tNachname\tVon\t\tBis\tAnzahl Pers.\tZimmernummer");
			System.out.println(
					"************************************************************************************************************");
			while (res.next()) {
				int buchungsnr = res.getInt("buchung_buchungsnr");
				String vorname = res.getString("vorname");
				String nachname = res.getString("nachname");
				int anzahlPers = res.getInt("anzahl_personen");
				int zimmerNr = res.getInt("zimmer.zimmer_zimmernr");
				Date von = res.getDate("von");
				Date bis = res.getDate("bis");
				System.out.println(buchungsnr + "		" + vorname + "      " + nachname + "      " + von + "      "
						+ bis + "      " + anzahlPers + "		" + zimmerNr);
			}
			System.out.println(
					"************************************************************************************************************");

			con.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
